package com.example.jintarkop;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

public class SetMonitoring extends AppCompatActivity {

    EditText nm_kopi, brt_basah, brt_kering;
    Button btn_save;

//    //connect firebase
//    FirebaseDatabase db = FirebaseDatabase.getInstance ();
//    DatabaseReference databaseReference;


    @Override
    protected void onCreate ( Bundle savedInstanceState ) {
        super.onCreate ( savedInstanceState );
        setContentView ( R.layout.activity_set_monitoring );

        //untuk memanggil fragment set monitoring
        Bundle bundle =  getIntent ().getExtras ();
        if (bundle != null){
            if (bundle.getString ( "some" ) != null) {
                Toast.makeText ( getApplicationContext (), "Silahkan Masukkan Nama Kopi dan Nilai " + bundle.getString ( "some" ), Toast.LENGTH_SHORT ).show ();
            }
        }

        //datainputan
        nm_kopi = findViewById ( R.id.nama_kopi );
        brt_basah = findViewById ( R.id.berat_basah );
        brt_kering = findViewById ( R.id.berat_kering );
        btn_save = findViewById ( R.id.btn_oke );
//        databaseReference = db.getReference ("Data ");
    }

//        public void  kirim_data(){
//
//            String  nm_kopitext = nm_kopi.getText ().toString ();
//            String brt_basahnumber = brt_basah.getText ().toString ();
//            String brt_keringnumber = brt_kering.getText ().toString ();
//
//
//        }

}
