package com.example.jintarkop;

public class Data {

    String data_namakopi, data_beratbasah, data_beratkering;
    String id_namakopi, id_beratbasah, id_beratkering;

    public void Data(){

    }


    public String getData_namakopi () {
        return data_namakopi;
    }

    public String getData_beratbasah () {
        return data_beratbasah;
    }

    public String getData_beratkering () {
        return data_beratkering;
    }

    public String getId_namakopi () {
        return id_namakopi;
    }

    public String getId_beratbasah () {
        return id_beratbasah;
    }

    public String getId_beratkering () {
        return id_beratkering;
    }

    public Data ( String data_namakopi , String data_beratbasah , String data_beratkering , String id_namakopi , String id_beratbasah , String id_beratkering ) {
        this.data_namakopi = data_namakopi;
        this.data_beratbasah = data_beratbasah;
        this.data_beratkering = data_beratkering;
        this.id_namakopi = id_namakopi;
        this.id_beratbasah = id_beratbasah;
        this.id_beratkering = id_beratkering;
    }
}
