package com.example.jintarkop;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.support.v4.app.NotificationCompat;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.messaging.FirebaseMessaging;


/**
 * A simple {@link Fragment} subclass.
 */
public class MonitorFragment extends Fragment {
//    private static final String TAG = "eaak";
    public String berat1, berat2, editBeratKopi, prakiraan1, cahaya, prakiraan2;
    private String[] namakopi = {
            "Kopi 2 ",
            "Kopi 1"
    };


    public MonitorFragment () {
        // Required empty public constructor
    }

    TextView data_berat1, data_berat2, txtPrakira, txtPrakira2;
    Button btn_databerat, btKering1, btKering2, btUpdate;
    EditText kering1, kering2, brt_kering;
    DatabaseReference reffdb;
    ProgressBar progressBar, progressBar2;


    @Override
    public View onCreateView ( LayoutInflater inflater , ViewGroup container ,
                               Bundle savedInstanceState ) {
        // Inflate the layout for this fragment
        View v = inflater.inflate ( R.layout.fragment_monitor , container , false );


        berat1 = "";
        berat2 = "";
        editBeratKopi= "1";
        data_berat1 = v.findViewById ( R.id.berat1 );
        data_berat2 = v.findViewById ( R.id.berat2 );
        brt_kering = v.findViewById( R.id.berat_kering );
        btn_databerat = v.findViewById ( R.id.btn_timbang );
        kering1= v.findViewById ( R.id.kering1 );
        kering2= v.findViewById ( R.id.kering );
        btKering1 = v.findViewById ( R.id.btn_kering );
        btKering2 = v.findViewById ( R.id.btn_kering2 );
        btUpdate = v.findViewById ( R.id.update );
        progressBar = v.findViewById ( R.id.progressBar );
        progressBar2 = v.findViewById ( R.id.progressBar2 );
        //final String toast = "Berat Kopi Berhasil Ditimbang";

        //textview perkiraan kering
        txtPrakira = v.findViewById(R.id.prakiraan1);
        txtPrakira2 = v.findViewById (R.id.prakiraan2);



        //Toast.makeText ( getContext (),toast,Toast.LENGTH_LONG ).show ();
        reffdb= FirebaseDatabase.getInstance ().getReference ();

        Spinner s = v.findViewById(R.id.spinnerJenis);
        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getActivity(), R.layout.support_simple_spinner_dropdown_item, namakopi);
        s.setAdapter(adapter);
        s.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                Toast.makeText(getContext(), "Jenis kopi: "+ adapter.getItem(i), Toast.LENGTH_SHORT).show();
                if( i == 1){

                    brt_kering.setText(berat1);
                    editBeratKopi = "1";
                }else {
                    brt_kering.setText(berat2);
                    editBeratKopi = "2";

                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        btn_databerat.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                reffdb.child ( "Pengering1" ).addValueEventListener ( new ValueEventListener () {
                    @Override
                    public void onDataChange ( @NonNull DataSnapshot dataSnapshot ) {
                        if (dataSnapshot.hasChild ( "Timbangan1" )){
                            String Timbangan1=dataSnapshot.child ( "Timbangan1" ).getValue ().toString ();
                            String Timbangan2=dataSnapshot.child ( "Timbangan2" ).getValue ().toString ();
                            berat1=dataSnapshot.child ( "beratKopiKering1" ).getValue ().toString ();
                            berat2=dataSnapshot.child ( "beratKopiKering2" ).getValue ().toString ();
                            data_berat1.setText ( Timbangan1 );
                            data_berat2.setText ( Timbangan2 );
                            brt_kering.setText(berat2);

                            cahaya = dataSnapshot.child("Cahaya").getValue().toString();

                            hitungPerkiraan(cahaya, Timbangan1, berat1);
                            hitungPerkiraan2(cahaya, Timbangan2, berat2);


                            Log.d ( "berat ==", berat1 );
                            Log.d ( "timbangan1 ==", Timbangan1);

                            int a = Integer.parseInt ( Timbangan1 );
                            int b = Integer.parseInt ( berat1 );
                            int hasil;
                            if ( a > b){
                                hasil = getPersenAir(a,b);
                                progressBar.setMax ( 100 );
                                progressBar.setProgress ( hasil );
                            } else {
                                progressBar.setMax ( 100 );
                                progressBar.setProgress ( 100 );
                            }


                            int as = Integer.parseInt ( Timbangan2 );
                            int bs = Integer.parseInt ( berat2 );
                            int hasils;
                            if ( as > bs){
                                hasils = getPersenAir(as,bs);
                                progressBar2.setMax ( 100 );
                                progressBar2.setProgress ( hasils );
                            } else {
                                progressBar2.setMax ( 100 );
                                progressBar2.setProgress ( 100 );
                            }

                            if (a < b){
                                Log.d ( "notif1 ==", berat1 );

                                MyFirebaseService.showNotification(getActivity (), "Pemberitahuan", "Kopi 1 Kering");
                            }

                            if (as < bs){
                                Log.d ( "notif2 ==", berat2 );

//                        Log.d ( "berat2 ==", Integer.parseInt(berat2));
//                        Log.d ( "timbangan2 ==",  Integer.parseInt(Timbangan2));

                                MyFirebaseService.showNotification(getActivity (), "Pemberitahuan", "Kopi 2 Kering");
                            }
                        }
                    }


                    @Override
                    public void onCancelled ( @NonNull DatabaseError databaseError ) {

                    }
                } );

            }
        } );

        btKering1.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( kering1.getText ().toString () )){
                    reffdb = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    reffdb.child ( "beratKopiKering1" ).setValue ( kering1.getText ().toString () );
                } else {
                    kering1.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Berat Kering1 Kosong" , Toast.LENGTH_SHORT ).show ();
                }

            }
        } );

        btKering2.setOnClickListener ( new View.OnClickListener () {
            @Override
            public void onClick ( View v ) {
                if (!TextUtils.isEmpty ( kering2.getText ().toString () )){
                    reffdb = FirebaseDatabase.getInstance ().getReference ().child ( "Pengering1" );
                    reffdb.child ( "beratKopiKering2" ).setValue ( kering2.getText ().toString () );

                }else {
                    kering2.setError ( "Harus diisi" );
                    Toast.makeText ( getActivity () , "Berat Kering2 Kosong" , Toast.LENGTH_SHORT ).show ();
                }

            }
        } );


        btUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editBeratKopi == "1"   ){
                    reffdb.child ( "Pengering1" ).child("beratKopiKering1").setValue(brt_kering.getText().toString());
                    Toast.makeText ( getActivity () , "Berat Kering 1 Berhasil Di Update" , Toast.LENGTH_SHORT ).show ();
                } else if(editBeratKopi == "2"){
                    reffdb.child ( "Pengering1" ).child("beratKopiKering2").setValue(brt_kering.getText().toString());
                    Toast.makeText ( getActivity () , "Berat Kering 2 Berhasil Di Update" , Toast.LENGTH_SHORT ).show ();
                } else {
                    Toast.makeText ( getActivity () , "Data Tidak Bisa Diupdate" , Toast.LENGTH_SHORT ).show ();
                }
            }
        });

        return v;


    }

    private int getPersenAir(int semuaData, int dataSekarang){
        float a = (float) semuaData;
        float b = (float) dataSekarang;
        float hasil = b/a * 100;
        return (int) hasil;
    }

    //perkiraan 1
    public void hitungPerkiraan(String Cahaya, String Timbangan1, String beratkopikering1){
        int cahaya = Integer.parseInt ( Cahaya );
        int timb1 = Integer.parseInt ( Timbangan1 );
        int brtKopi = Integer.parseInt ( beratkopikering1 );
        int hari = 1;
        Log.d("cahaya =", Integer.toString(cahaya));

        if(cahaya > 50){
            int berkurang = timb1*15/100;
            int selisih = timb1 - brtKopi;
            Log.d("selisih =", Integer.toString(selisih));

            for(int i = berkurang; i < selisih; i++){
                hari = hari + 1;
                if (hari > 1){
                    i = i + berkurang;
                }
                Log.d("hasil i =", Integer.toString(i));
                Log.d("hasil hr =", Integer.toString(hari));

                prakiraan1 ="Perkiraan kopi kering dalam " + Integer.toString(hari) + " hari";

            }
        } else {
            int berkurang = timb1*5/100;
            int selisih = timb1 - brtKopi;
            Log.d("selisih =", Integer.toString(selisih));

            for(int i = berkurang; i < selisih; i++){
//                int has = timb1 - berkurang;
                hari = hari + 1;
                if (hari > 1){
                    i = i + berkurang;
                }
                Log.d("hasil i =", Integer.toString(berkurang));
                Log.d("hasil hr =", Integer.toString(hari));
                prakiraan1 ="Perkiraan kopi kering dalam " + Integer.toString(hari) + " hari";

            }
        }

        txtPrakira.setText(prakiraan1);

    }



    //perkiraan2

    public void hitungPerkiraan2(String Cahaya, String Timbangan2, String beratkopikering2){
        int cahaya = Integer.parseInt ( Cahaya );
        int timb2 = Integer.parseInt ( Timbangan2 );
        int brtKopi2 = Integer.parseInt ( beratkopikering2 );
        int hari = 1;
        Log.d("cahaya =", Integer.toString(cahaya));

        if(cahaya > 50){
            int berkurang = timb2*15/100;
            int selisih = timb2 - brtKopi2;
            Log.d("selisih =", Integer.toString(selisih));

            for(int i = berkurang; i < selisih; i++){
//                int has = timb1 - berkurang;
                hari = hari + 1;
                if (hari > 1){
                    i = i + berkurang;
                }
                Log.d("hasil i =", Integer.toString(i));
                Log.d("hasil hr =", Integer.toString(hari));

                prakiraan2="Perkiraan kopi kering dalam " + Integer.toString(hari) + " hari";

            }
        } else {
            int berkurang = timb2*5/100;
            int selisih = timb2 - brtKopi2;
            Log.d("selisih =", Integer.toString(selisih));

            for(int i = berkurang; i < selisih; i++){
//                int has = timb1 - berkurang;
                hari = hari + 1;
                if (hari > 1){
                    i = i + berkurang;
                }
                Log.d("hasil i =", Integer.toString(berkurang));
                Log.d("hasil hr =", Integer.toString(hari));
                prakiraan2 ="Perkiraan kopi kering dalam " + Integer.toString(hari) + " hari";

            }
        }

        txtPrakira2.setText(prakiraan2);
    }




}
